var IsPackageInstalled = require('alpi_basis').System.IsPackageInstalled;
var ShellCommand = require('alpi_basis').System.ShellCommand;

module.exports = {
	/**
	 * Detects the services manager.
	 * 
	 * @param {Function} callback
	 */
	ServicesManager: function (callback) {
		IsPackageInstalled('systemd', (e) => {
			if (e === true) {
				callback('systemd');
			} else {
				IsPackageInstalled('service', (e) => {
					if (e === true) {
						callback('service');
					} else {
						callback('unknown');
					}
				});
			}
		});
	},
	/**
	 * Retrieves the list of the available services.
	 * 
	 * @param {Function} callback 
	 */
	List: function (callback) {
		module.exports.ServicesManager((ServicesManager)=>{
			if(ServicesManager === 'systemd'){
				ShellCommand('systemctl list-unit-files -alt service --no-legend --no-pager | cut -d \' \' -f1 | tr -d \'@\'', (services) => {
					callback(services.error === null ? services.stdout.trim().split('\n') : false);
				});
			}else{
				callback(false);
			}
		});
	},
	/**
	 * Retrieves the list of the available services with their basic informations
	 * @param {Function} callback 
	 */
	ListWithInformations: function(callback){
		module.exports.List((services)=>{
			if(services !== false){
				var p = [];
				for(let i = 0; i < services.length; i++){
					p.push(new Promise((resolve)=>{
						ShellCommand(`systemctl show ${services[i]} --no-pager -p Names,Description,LoadState,ActiveState,SubState,UnitFileState --value`, (informations) => {
							if (informations.error === null) {
								informations = informations.stdout.trim().split('\n');
								resolve({
									unit: informations[0],
									description: informations[1],
									load: informations[2],
									active: informations[3],
									sub: informations[4],
									state: informations[5]
								});
							} else {
								resolve(false);
							}
						});
					}));
				}
				Promise.all(p).then((results)=>{
					if(results.every((result)=>{ return result !== false; })){
						var result = {};
						for(let i = 0; i < services.length; i++){
							result[services[i]] = results[i];
						}
						callback(result);
					}else{
						callback(false);
					}
				});
			}else{
				callback(false);
			}
		});
	},
	/**
	 * Checks if a service exists.
	 * 
	 * @param {String} service 
	 * @param {Function} callback 
	 */
	Exists: function (service, callback) {
		module.exports.List((services)=>{
			if(services !== false){
				var found = false;
				for(var i = 0; i < services.length; i++){
					if(services[i] === service || services[i].split('.')[0] === service){
						found = true;
						break;
					}
				}
				callback(found);
			}else{
				callback(false);
			}
		});
	},
	/**
	 * Retrieves basic informations on a service.
	 * @param {String} service 
	 * @param {Function} callback 
	 */
	Basic: function (service, callback) {
		module.exports.Exists(service, (exists) => {
			if (exists === true) {
				ShellCommand(`systemctl show ${service} --no-pager -p Names,Description,LoadState,ActiveState,SubState,UnitFileState --value`, (informations) => {
					if (informations.error === null) {
						informations = informations.stdout.trim().split('\n');
						callback({
							unit: informations[0],
							description: informations[1],
							load: informations[2],
							active: informations[3],
							sub: informations[4],
							state: informations[5]
						});
					} else {
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	},
	/**
	 * Retrieves detailed informations on a service.
	 * @param {String} service 
	 * @param {Function} callback 
	 */
	Detailed: function(service, callback){
		module.exports.Exists(service, (exists)=>{
			if(exists === true){
				ShellCommand(`systemctl show ${service} --no-pager`, (informations)=>{
					if(informations.error === null){
						informations = informations.stdout.trim().split('\n');
						var results = {};
						for(let i = 0; i < informations.length; i++){
							var information = informations[i].split('=');
							results[information[0]] = information[1];
						}
						callback(results);
					}else{
						callback(false);
					}
				});
			}else{
				callback(false);
			}
		});
	},
	/**
	 * Retrieves the load of a service.
	 * 
	 * @param {String} service 
	 * @param {Function} callback 
	 */
	IsActive: function(service, callback){
		module.exports.Exists(service, (exists)=>{
			if(exists === true){
				ShellCommand(`systemctl is-active ${service}`, (isActive)=>{
					if(isActive.stderr === ''){
						callback(isActive.stdout.trim());
					}else{
						callback(false);
					}
				});
			}else{
				callback(false);
			}
		});
	},
	/**
	 * Retrieves the state of the service.
	 * 
	 * @param {String} service 
	 * @param {Function} callback 
	 */
	IsEnabled: function(service, callback){
		module.exports.Exists(service, (exists) => {
			if (exists === true) {
				ShellCommand(`systemctl is-enabled ${service}`, (isActive) => {
					if (isActive.stderr === '') {
						callback(isActive.stdout.trim());
					} else {
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	},
	/**
	 * List the services needed for the service to boot.
	 * 
	 * @param {String} service 
	 * @param {Function} callback 
	 */
	DependsOn: function(service, callback){
		module.exports.Exists(service, (exists)=>{
			if(exists === true){
				ShellCommand(`systemctl ${service} --plain  | grep .service | tr -d' '`, (dependencies)=>{
					if(dependencies.error === null){
						dependencies = dependencies.stdout.trim();
						dependencies.shift();
						callback(dependencies);
					}else{
						callback(false);
					}
				});
			}else{
				callback(false);
			}
		});
	},
	/**
	 * List the services that needs the service to boot.
	 * @param {String} service 
	 * @param {Function} callback 
	 */
	RequiredBy: function(service, callback){
		module.exports.Exists(service, (exists) => {
			if (exists === true) {
				ShellCommand(`systemctl ${service} --reverse --plain  | grep .service | tr -d' '`, (dependents) => {
					if (dependents.error === null) {
						dependents = dependents.stdout.trim();
						dependents.shift();
						callback(dependents);
					} else {
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	}
};
