var ShellCommand = require('alpi_basis').System.ShellCommand;
var IsActive = require('./informations').IsActive;
var IsEnabled = require('./informations').IsEnabled;

module.exports = {
	/**
     * Starts a service.
     * 
     * @param {String} service
     * @param {Function} callback
     */
	Start: function (service, callback) {
		IsEnabled(service, (enabled)=>{
			if(enabled === false || enabled === 'masked'){
				callback(false);
			}else{
				IsActive(service, (active)=>{
					if(active === 'false'){
						callback(false);
					}else if(active === 'active'){
						callback(true);
					}else{
						ShellCommand(`sudo systemctl start ${service}`, (started)=>{
							callback(started.error === null);
						});
					}
				});
			}
		});
	},
	/**
     * Stops a service.
     * 
     * @param {String} service
     * @param {Function} callback
     */
	Stop: function (service, callback) {
		IsActive(service, (active) => {
			if (active === 'false') {
				callback(false);
			} else if (active === 'inactive') {
				callback(true);
			} else {
				ShellCommand(`sudo systemctl stop ${service}`, (stopped) => {
					callback(stopped.error === null);
				});
			}
		});
	},
	/**
     * Restarts a service.
     * 
     * @param {String} service
     * @param {Function} callback
     */
	Restart: function (service, callback) {
		IsEnabled(service, (enabled) => {
			if (enabled === false || enabled === 'masked') {
				callback(false);
			} else {
				IsActive(service, (active) => {
					if (active === 'false') {
						callback(false);
					} else if (active === 'active') {
						ShellCommand(`sudo systemctl restart ${service}`, (restarted) => {
							callback(restarted.error === null);
						});
					} else {
						callback(false);
					}
				});
			}
		});
	},
	/**
     * Enables a service.
     * 
     * @param {String} service
     * @param {Function} callback
     */
	Enable: function (service, callback) {
		IsEnabled(service, (enabled) => {
			if (enabled === false || enabled === 'masked') {
				callback(false);
			} else if (enabled === 'enabled') {
				callback(true);
			} else {
				ShellCommand(`sudo systemctl enable ${service}`, (masked) => {
					callback(masked.error === null);
				});
			}
		});
	},
	/**
     * Disables a service.
     * 
     * @param {String} service
     * @param {Function} callback
     */
	Disable: function (service, callback) {
		IsEnabled(service, (enabled) => {
			if (enabled === false || enabled === 'masked') {
				callback(false);
			} else if (enabled === 'disabled') {
				callback(true);
			} else {
				ShellCommand(`sudo systemctl disable ${service}`, (masked) => {
					callback(masked.error === null);
				});
			}
		});
	},
	/**
	 * Masks a service.
	 * 
	 * @param {String} service 
	 * @param {Function} callback 
	 */
	Mask: function(service, callback){
		IsEnabled(service, (enabled) => {
			if (enabled === false) {
				callback(false);
			} else if(enabled === 'masked') {
				callback(true);
			} else {
				ShellCommand(`sudo systemctl mask ${service}`, (masked) => {
					callback(masked.error === null);
				});
			}
		});
	},
	/**
	 * Unmasks a service.
	 * 
	 * @param {String} service 
	 * @param {Function} callback 
	 */
	Unmask: function(service, callback){
		IsEnabled(service, (enabled) => {
			if (enabled === false) {
				callback(false);
			} else if (enabled === 'masked') {
				ShellCommand(`sudo systemctl unmask ${service}`, (masked) => {
					callback(masked.error === null);
				});
			} else {
				callback(true);
			}
		});
	}
};
